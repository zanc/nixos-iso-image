;; Fix "Bad Request" issue.
(when (version< emacs-version "26.3")
  (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

;; Prevent package.el loading packages prior to their init-file loading
(when (version<= "27" emacs-version)
  (setq package-enable-at-startup nil))

;; Display
(menu-bar-mode 0)           ;; hides menu bar
(tool-bar-mode 0)           ;; hides tool bar
(blink-cursor-mode 0)       ;; disables cursor blinking
(scroll-bar-mode 0)         ;; disables scroll bar

;; Frames
(add-to-list 'default-frame-alist '(font . "Fira Code-10"))
;;(add-to-list 'default-frame-alist '(alpha . 75)) ;; transparency

;; Mode Line
(line-number-mode 1)
(column-number-mode 1)
(size-indication-mode 1)

;; Set backup locations
(setq backup-directory-alist
      `((".*" . ,(locate-user-emacs-file "backups/"))))
(setq auto-save-file-name-transforms
      `((".*" ,(locate-user-emacs-file "backups/") t)))
(customize-set-variable
 'tramp-backup-directory-alist backup-directory-alist)

;; Highlight parentheses
(setq show-paren-delay 0)
(show-paren-mode 1)
;;(setq blink-matching-delay 0)

;; Enable line numbers
(when (version<= "26.0.50" emacs-version)
  (progn
    (require 'display-line-numbers)
    (defun display-line-numbers--turn-on ()
       "Turn on line numbers but excempting certain majore modes."
       (if (and
            (not (member major-mode '(help-mode Info-mode ibuffer-mode dired-mode occur-mode neotree-mode)))
            (not (minibufferp)))
           (display-line-numbers-mode)))
    (global-display-line-numbers-mode)))

;; Disregard read-only-mode
;;(setq inhibit-read-only t)

;; Disable large file warning
;;(setq large-file-warning-threshold nil)

;; Remember point position
(if (version< emacs-version "25.1")
    (progn
      (require 'saveplace)
      (setq-default save-place t)
      (setq save-place-file (locate-user-emacs-file "places")))
  (save-place-mode 1))

;; File completions
(global-set-key (kbd "C-c C-f")
                'comint-dynamic-complete-filename
                ;;'comint-replace-by-expanded-filename
                )

;; Don't insert tabs
(setq-default indent-tabs-mode nil)

;; Disable line wrap
(setq-default truncate-lines t)
(set-display-table-slot standard-display-table 'truncation 32)
(fringe-mode 0)

;; Scroll one line at a time
(setq scroll-step 1)

;; Hide all buffers with an asterisk in ibuffer
(require 'ibuf-ext)
(add-to-list 'ibuffer-never-show-predicates "^\\*")

;; Prevent M-/ looking in other buffers
(setq dabbrev-check-other-buffers nil)

;; Enable upper case and lower case conversion commands
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; Move point to the bottom line when reaching end of buffer
(setq scroll-error-top-bottom t)

;; Preserve point position when scrolling
(setq scroll-preserve-screen-position t)

;; Rebind M-DEL to its original function in diff-mode
(add-hook 'diff-mode-hook (lambda ()
                            (local-unset-key (kbd "M-DEL"))))

;; Don't save auth info to ~/.authinfo
(setq auth-source-save-behavior nil)

;; Treat alacritty like screen
;;(add-to-list 'term-file-aliases '("alacritty" . "screen"))
;;(add-to-list 'term-file-aliases '("alacritty" . "xterm"))

;; Set CC Mode indentation
;;(add-hook 'c-mode-common-hook (lambda ()
;;                                (setq c-basic-offset 4)))

;; Set sh-script mode indentation
;;(add-hook 'sh-mode-hook (lambda ()
;;                          (setq sh-basic-offset 2)))

;; Fix TRAMP hanging
(setq tramp-shell-prompt-pattern (concat "\\(?:^\\|\r\\)"
                                         "[^]#$%>\n]*#?[]#$%>].* *\\(\e\\[[0-9;]*[a-zA-Z] *\\)*"))

;; Insert numbers from n to m
(defun insert-number (n m)
  (dolist (x (number-sequence n m))
    (insert (concat (int-to-string x) "\n"))))

;; Case-insensitive file name completion
(setq read-file-name-completion-ignore-case t)

;; Case-insensitive buffer completion
(setq read-buffer-completion-ignore-case t)

;; Use the primary selection for kill and yank commands.
;; To access the clipboard:
;;   clipboard-kill-region
;;   clipboard-kill-ring-save
;;   clipboard-yank
;;(setq select-enable-clipboard nil
;;      select-enable-primary t
;;      mouse-drag-copy-region t)

;; Minibuffer completion at point
;;(define-key minibuffer-local-map (kbd "M-p") 'previous-complete-history-element)
;;(define-key minibuffer-local-map (kbd "M-n") 'next-complete-history-element)
;;(define-key minibuffer-local-map (kbd "<up>") 'previous-complete-history-element)
;;(define-key minibuffer-local-map (kbd "<down>") 'next-complete-history-element)

;; Save minibuffer history
(savehist-mode 1)

;; Copy the region into the clipboard using xclip
(defun xclip-region ()
  (interactive)
  (unless (executable-find "xclip")
    (error "xclip not found in exec-path"))
  (shell-command-on-region (region-beginning)
                           (region-end)
                           "xclip -selection clipboard >/dev/null 2>&1"
                           nil
                           nil
                           nil
                           nil))

;; Copy the string into the clipboard using xclip
(defun xclip-string (input)
  (interactive (list (read-shell-command "String input: ")))
  (unless (executable-find "xclip")
    (error "xclip not found in exec-path"))
  (shell-command (concat "printf " (shell-quote-argument input) " | xclip -selection clipboard >/dev/null 2>&1")))

;; Copy the kill ring into the clipboard using xclip
(defun xclip-kill-ring ()
  (interactive)
  (unless (executable-find "xclip")
    (error "xclip not found in exec-path"))
  (shell-command (concat "printf " (shell-quote-argument (current-kill 0)) " | xclip -selection clipboard >/dev/null 2>&1")))

;; Forward region to 0x0.st
(defun 0x0 ()
  (interactive)
  (unless (executable-find "curl")
    (error "curl not found in exec-path"))
  (shell-command-on-region (region-beginning)
                           (region-end)
                           "curl -F file=@- https://0x0.st/"
                           nil
                           nil
                           nil
                           nil))

;; Do not convert tag names to words in Customize, i.e. foo-bar-baz -> "Foo Bar Baz"
(setq custom-unlispify-tag-names nil)

;; Truncate long buffer name in mode line
(setq-default mode-line-buffer-identification (append '(-20) (propertized-buffer-identification "%12b")))

;; Set bash history size
(setenv "HISTSIZE" "100000")
(setenv "HISTFILESIZE" (getenv "HISTSIZE"))

;; NFO viewer
;; (revert-buffer-with-coding-system 'cp437)

;; Set JavaScript mode indentation
(setq js-indent-level 2)

;; Use fundamental-mode if the line is too overly long
(add-hook 'find-file-hook
          #'(lambda ()
              (unless (or (= (point-max) 1)
                          (string-match-p "\n" (buffer-substring-no-properties
                                                (point-min)
                                                (if (> (point-max) 1000) 1000 (point-max)))))
                (fundamental-mode)
                (setq truncate-lines nil)
                (highlight-indent-guides-mode 0))))
