# nixos-iso-image

A custom NixOS ISO with my personal configurations included.

## Building

For `nix` with flake support:

```
$ nix build
```

otherwise:

```
$ nix-build
```

## Setting up a USB

To setup a USB with a separate partition for storage:

1. Partition the USB using fdisk, i.e. make /dev/sdX1 (type: 0xc) and /dev/sdX2 (type: 0x7).

2. Create the filesystems:

```
# mkfs.vfat -F32 -n nixos-iso /dev/sdX1
# mkfs.ntfs -f -L storage /dev/sdX2
```

3. Mount the /dev/sdX1 and copy the contents of the ISO into it:

```
# mkdir -p /mnt/iso /mnt/loop
# mount /dev/sdX1 /mnt/iso
# mount -t iso9660 -o loop,ro result/iso/nixos-*.iso /mnt/loop
# find /mnt/loop -mindepth 1 -maxdepth 1 -exec cp -R {} /mnt/iso \;
```

4. Rename isolinux to syslinux:

```
# mv /mnt/iso/isolinux /mnt/iso/syslinux
# mv /mnt/iso/syslinux/isolinux.cfg /mnt/iso/syslinux/syslinux.cfg
# sed -i -e 's,\(/isolinux\)\(/background\.png\),/syslinux\2,' /mnt/iso/syslinux/syslinux.cfg
```

5. Change the ISO label to look for, since the original label is greater than 11 bytes.

```
# sed -i -e 's,\(root=LABEL=\)[^ ]*,\1nixos-iso,' /mnt/iso/syslinux/syslinux.cfg
```

6. Unmount everything:

```
# umount /mnt/iso /mnt/loop
```

7. Install syslinux onto the USB:

```
# syslinux --install --directory syslinux /dev/sdX1
```

8. Make syslinux recognize our partition (the one that contains the ISO contents):

```
# syslinux=$(nix-build --no-out-link '<nixpkgs>' -A syslinux)
# # '\x1' refers to sdX1
# printf '\x1' | cat $syslinux/share/syslinux/altmbr.bin - | dd of=/dev/sdX bs=440 count=1 iflag=fullblock
```
