rec {
  description = "Custom NixOS ISO image";

  inputs = {
    nixpkgs.url = "https://releases.nixos.org/nixos/24.11/nixos-24.11.714876.5d7db4668d7a/nixexprs.tar.xz";
    nixpkgs-overlays = { url = "gitlab:zanc/overlays/83a483faba58a1d9299d7a782b8606f75bd2f3a4"; flake = false; };
  };

  outputs = { self, nixpkgs, nixpkgs-overlays }:

    let

      supportedSystems = [ "x86_64-linux" "i686-linux" "aarch64-linux" ];

      forAllSystems = f: nixpkgs.lib.genAttrs supportedSystems (system: f system);

      makeSystem = { system, modules }:
        import (nixpkgs + "/nixos/lib/eval-config.nix") {
          inherit system modules;
        };

      nixosFor = forAllSystems (system: makeSystem {
        inherit system;
        modules = [ self.nixosModule ];
      });

    in {

      defaultPackage = forAllSystems (system:
        nixosFor.${system}.config.system.build.isoImage
      );

      nixosModule = ({ options, ... }: {
        imports = [ ./iso.nix ];

        # Make pinned flakes available as <path>.
        nix.nixPath = options.nix.nixPath.default ++ [
          "nixpkgs-overlays=${nixpkgs-overlays}/overlays.nix"
        ];

        # Set the overlays.
        nixpkgs.overlays = import (nixpkgs-overlays + "/overlays.nix");

        system.defaultChannel = builtins.dirOf inputs.nixpkgs.url;
      });

      #inherit self; # debug ;)

    };
}
