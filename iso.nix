{ options, lib, pkgs, modulesPath, ... }:

{

  imports =
    [ (modulesPath + "/installer/cd-dvd/installation-cd-minimal.nix")
    ];

  nixpkgs.config.allowUnfree = true;

  hardware.enableAllFirmware = true;

  services.logind.lidSwitch = "ignore";

  nix.package = pkgs.nixUnstable;

  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';
}

// (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "22.05"
    then {
      nix.settings.trusted-users = [ "root" "nixos" ];
    }
    else {
      nix.trustedUsers = options.nix.trustedUsers.default ++ [ "nixos" ];
    })

// {

  users.users.nixos.initialHashedPassword = lib.mkForce "$6$tQqopSpo/sboTlOm$V3tZT0r0gE0A6GKVYyRWFLChGm1fIhwUEyjxajtHztdZA1GJN5aGgcoiSNE2DE7XBWDTerIo4sB7auQ2fmcO0/";

  users.users.root.initialHashedPassword = lib.mkForce "$6$w7gTiWE3I8tLFLLG$floTHpqkz0hLJqW3.1NQZXHew8feRluZN9n03r49zGaN7FxtssummlSpojoNhp511SYntOG6sLHjnCy8STuyI1";

  environment.systemPackages = with pkgs; [
    wimlib
    dosfstools
    git
  ];

  services.emacs.enable = true;

  services.gpm.enable = true;

  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";

  programs.gnupg.agent.enable = true;

  programs.bash.shellInit = ''
    if [ ! -e /home/nixos/.emacs ]; then
      if [ -w /home/nixos ]; then
        cp /iso/.emacs /home/nixos
        chmod +w /home/nixos/.emacs
      fi
    fi
  '';

  isoImage.contents =
    [
      { source = pkgs.writeText "dot-emacs" (builtins.readFile ./.emacs);
        target = "/.emacs";
      }
    ];

  isoImage.volumeID = "nixos-iso";

}
